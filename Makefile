build:
	: Lütfen sudo make install komutunu çalıştırın.
install:

	install masaustu_kisayol_olustur.nemo_action /usr/share/nemo/actions/masaustu_kisayol_olustur.nemo_action
	install masaustu_kisayol_olustur.sh /usr/share/nemo/actions/masaustu_kisayol_olustur.sh

	install pdf_ayir.nemo_action /usr/share/nemo/actions/pdf_ayir.nemo_action
	install pdf_ayir.sh /usr/share/nemo/actions/pdf_ayir.sh

	install pdf_birlestir.nemo_action /usr/share/nemo/actions/pdf_birlestir.nemo_action
	install pdf_birlestir.sh /usr/share/nemo/actions/pdf_birlestir.sh

	install pdfye_donustur_tekli.nemo_action /usr/share/nemo/actions/pdfye_donustur_tekli.nemo_action
	install pdfye_donustur_coklu.nemo_action /usr/share/nemo/actions/pdfye_donustur_coklu.nemo_action
	install pdfye_donustur.sh /usr/share/nemo/actions/pdfye_donustur.sh

	install pdf_boyut_dusurme.nemo_action /usr/share/nemo/actions/pdf_boyut_dusurme.nemo_action
	install pdf_boyut_dusurme.sh /usr/share/nemo/actions/pdf_boyut_dusurme.sh

	install pdfye_ocr_uygula.nemo_action /usr/share/nemo/actions/pdfye_ocr_uygula.nemo_action
	install pdfye_ocr_uygula.sh /usr/share/nemo/actions/pdfye_ocr_uygula.sh

	install qrcode_oku.nemo_action /usr/share/nemo/actions/qrcode_oku.nemo_action
	install qrcode_oku.py /usr/share/nemo/actions/qrcode_oku.py

	install pdftojpg.nemo_action /usr/share/nemo/actions/pdftojpg.nemo_action
	install pdftojpg.sh /usr/share/nemo/actions/pdftojpg.sh

	install yazdir.nemo_action /usr/share/nemo/actions/yazdir.nemo_action
	install yazdir.sh /usr/share/nemo/actions/yazdir.sh	
