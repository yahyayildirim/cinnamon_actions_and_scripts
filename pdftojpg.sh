#!/bin/bash
#
#####################################
# PDF Ayırma Scripti                #
# Yahya YILDIRIM                    #
# kod.pardus.org.tr/yahyayildirim   #
#####################################
#
dizin=`dirname "$1"`

#
# Dosyanın hangi isim ile kaydedileceğini sor
if ! DOSYA_ADI=$(zenity --entry \
  --title="PDF Dosyalarını JPG'e Çevir" \
  --text="Yeni oluşacak JPG uzantılı dosyaya bir isim verin." \
  --width=350); then
  exit 1
fi
#
# Dosyaları ayırmak için img2pdf programı kullanılacak
if [[ $DOSYA_ADI == "" ]]; then
  notify-send --expire-time=200000 "Dosya ismi boş olamaz. Tekrar deneyin."
  exit 1
elif [[ -e "$dizin/${DOSYA_ADI// /_}.jpg" ]]; then
  notify-send --expire-time=200000 "Aynı isimli dosya zaten var. Lütfen başka bir isim ile tekrar deneyin."
  exit 1
else
  notify-send --expire-time=200000 "PDF dosyası başarılı bir şekilde oluşturuldu."
  pdftoppm -jpeg "$@" "$dizin/${DOSYA_ADI// /_}"
  exit 0
fi
