#!/bin/bash

IFS=$'\n'
YAZICI=$(lpstat -d | grep -Eo "\S+$")
DOSYALAR=$(realpath "$@")
UZANTI="PDF|PNG|JPEG|SVG|OpenDocument|UTF-8|ASCII|Microsoft|Windows"

for DOSYA in ${DOSYALAR[@]}; do
    dosyaTuru=$(file -b "$DOSYA" | grep -Eo "$UZANTI")
    case $dosyaTuru in
        OpenDocument|Microsoft|Windows )
            libreoffice --pt $YAZICI "$DOSYA"
        ;;
        PDF|PNG|JPEG|UTF-8|ASCII )
            lpr -P $YAZICI "$DOSYA"
        ;;
        SVG )
            # imagemagick yüklü olması gerekiyor.
            convert "$DOSYA" png:- | lpr -P $YAZICI
        ;;
        * )
            notify-send -i error "'$DOSYA' yazdırılmadı."
        ;;
    esac
    [ $? -eq 0 ] || notify-send -i error "Yazdırma hatası: '$DOSYA'"
done
